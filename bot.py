#!/usr/bin/env python3

import logging
from os import environ
from telegram.ext import Updater, Filters, MessageHandler
from telegram.ext.dispatcher import run_async

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


@run_async
def error_handler(bot, update, error):
    logger.exception(error)


@run_async
def all_handler(bot, update):
    return update.message.reply_text('```\n%s\n```' % str(
        update.message), parse_mode='markdown', quote=True)


if __name__ == '__main__':
    updater = Updater(environ.get('TOKEN'), workers=10)
    dp = updater.dispatcher
    dp.add_handler(MessageHandler(Filters.all, all_handler))
    dp.add_error_handler(error_handler)
    updater.start_polling()
    updater.idle()
